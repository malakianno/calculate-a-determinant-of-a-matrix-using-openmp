***This application calculate a determinant of a matrix with using OpenMP and without it.
***
### Attention!!! Don't forget to change the path of the input file!!! ###

**Format of the input file:**

**Name:** matrix.dat

**Data:**

size of matrix

number non null elements in the 1st row

column number and the value of the 1st non null element in the 1st row

column number and the value of the 2nd non null element in the 1st row

...

number non null elements in the 2nd row

column number and the value of the 1st non null element in the 2nd row

column number and the value of the 2nd non null element in the 2nd row

...

For example:

3

2

1 1

3 -1

1

2 7

3

1 1

2 3

3 -2


You will get next matrix:

1 0 -1

0 7  0

1 3 -2