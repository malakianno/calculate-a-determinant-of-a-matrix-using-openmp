#-------------------------------------------------
#
# Project created by QtCreator 2015-03-10T16:29:06
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = openMP_minor_of_matr
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp

QMAKE_CXXFLAGS += -fopenmp
LIBS += -fopenmp
