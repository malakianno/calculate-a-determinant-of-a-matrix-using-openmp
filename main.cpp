/*
 * This application count the determinant of the matrix with using OpenMP
 * and without it.
 * Format of the input file:
 * Name: matrix.dat
 * Data:
 * size of matrix
 * number non null elements in the 1st row
 * column number and the value of the 1st non null element in the 1st row
 * column number and the value of the 2nd non null element in the 1st row
 * ...
 * number non null elements in the 2nd row
 * column number and the value of the 1st non null element in the 2nd row
 * column number and the value of the 2nd non null element in the 2nd row
 * ...
 * For example:
 * 3
 * 2
 * 1 1
 * 3 -1
 * 1
 * 2 7
 * 3
 * 1 1
 * 2 3
 * 3 -2
 * You will get next matrix:
 * 1 0 -1
 * 0 7  0
 * 1 3 -2
 * */

#include <iostream>
#include <cmath>
#include <omp.h>
#include <fstream>

using namespace std;

int determinant_without_using_openmp(int** arr, int size)
{
    int i, j;
    int det = 0;
    int ** m;
    if(size == 1)
        det = arr[0][0];
    else
        if(size == 2)
            det = arr[0][0]*arr[1][1] - arr[0][1]*arr[1][0];
        else
            {
                m = new int*[size - 1];
                for(i = 0; i < size; ++i)
                {
                    for(j = 0; j < size - 1; ++j)
                        if(j < i)
                            m[j] = arr[j];
                        else m[j] = arr[j + 1];
                    det += pow((double)(-1), (i + j))*determinant_without_using_openmp(m, size - 1)*arr[i][size - 1];
                }
                delete [] m;
            }
    return det;
}

int determinant_with_using_openmp(int** arr, int size)
{
        int det = 0;
        if(size == 1)
            det = arr[0][0];
        else
            if(size == 2)
                det = arr[0][0]*arr[1][1] - arr[0][1]*arr[1][0];
            else
            {
                #pragma omp parallel shared(arr, size)
                {
                    int i, j;
                    int ** m;
                    m = new int*[size - 1];
                    #pragma omp for reduction(+: det) schedule(auto)
                    for(i = 0; i < size; i++)
                    {
                        for(j = 0; j < size - 1; j++)
                        {
                            if(j < i)
                                m[j] = arr[j];
                            else
                                m[j] = arr[j + 1];
                        }
                        det += pow((double)(-1), (i + j))*determinant_without_using_openmp(m, size - 1)*arr[i][size - 1];
                    }
                    delete [] m;
                 }
            }

    return det;
}

int main()
{
    int N = 1;
    fstream F;
    F.open("//home//malakianno//Qt projects//openMP_minor_of_matr//matrix.dat", ios::in);
    if(F.is_open() == false)
    {
        cout << "Error. File not found!" << endl;
        return 0;
    }
    F>>N;
    int ** matrix = new int*[N];
    int ** temp = new int*[N - 1];
    for (int i = 0; i < N; i++) {
                matrix[i] = new int[N];
                temp[i] = new int[N - 1];
            }

    for(int row_num = 0; !F.eof(); row_num++)
    {
        int count_elements = 0;
        F>>count_elements;
        for(int i =0; i < count_elements; i++)
        {
            int column_num = 0;
            F>>column_num;
            int value = 0;
            F>>value;
            matrix[row_num][column_num - 1] = value;
        }
    }
    F.close();

    double start_time = omp_get_wtime();
    int determinant = determinant_without_using_openmp(matrix, N);
    double end_time = omp_get_wtime();
    cout << "Results without using OpenMP: " << endl;
    cout << "Determinant: " << determinant << endl;
    cout << "Work time: " << end_time - start_time << endl;

    cout << "----------------------------------------------" << endl;

    start_time = omp_get_wtime();
    determinant = determinant_with_using_openmp(matrix, N);
    end_time = omp_get_wtime();
    cout << "Results with using OpenMP: " << endl;
    cout << "Determinant: " << determinant << endl;
    cout << "Work time: " << end_time - start_time << endl;

    delete [] matrix;
    return 0;
}


